#include "encompass.h"

#include <gtkhtml/gtkhtml-embedded.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-mime-info.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

void encompass_title_changed(GtkHTML * html,
			     const gchar * title,
			     Encompass_Objects * browser) {
  gchar * s;
  BonoboArg * arg;
  CORBA_Environment ev;

  CORBA_exception_init (&ev);
  s = g_strconcat ("Encompass: ", title, NULL);
  arg = bonobo_arg_new (BONOBO_ARG_STRING);
  BONOBO_ARG_SET_STRING (arg, s);

  bonobo_property_bag_notify_listeners (browser->pb, "title", arg, &ev);
  if (BONOBO_EX (&ev)) {
    g_warning ("Notify listeners exception: %s\n",
	       bonobo_exception_get_text (&ev));
  }

  bonobo_arg_release (arg);
  CORBA_exception_free (&ev);
  g_free (s);
}

void encompass_set_base(GtkHTML * html,
			gchar * base,
			Encompass_Objects * browser) {
  BonoboArg * arg;
  CORBA_Environment ev;
  
  CORBA_exception_init (&ev);

  if (baseURL)
    html_url_destroy (baseURL);

  baseURL = html_url_new (base);

  arg = bonobo_arg_new (BONOBO_ARG_STRING);
  BONOBO_ARG_SET_STRING (arg, base);

  bonobo_property_bag_notify_listeners (browser->pb, "url", arg, &ev);
  if (BONOBO_EX (&ev)) {
    g_warning ("Notify listeners exception: %s\n", bonobo_exception_get_text (&ev));
  }

  bonobo_arg_release (arg);

  CORBA_exception_free (&ev);
}

gboolean encompass_object_requested(GtkHTML * html,
				   GtkHTMLEmbedded * eb,
				   Encompass_Objects * browser) {
  if(!eb->classid)
    return FALSE;

  if(!strncasecmp(eb->classid, "OAFIID:", strlen("OAFIID:"))) {
    GtkWidget * component = bonobo_widget_new_control(eb->classid,
						      CORBA_OBJECT_NIL);
    gtk_widget_show(component);
    gtk_container_add(GTK_CONTAINER(eb), component);
  }

  printf("CLASS ID: %s\n", eb->classid);
  printf("Object Name: %s\n", eb->name);
  printf("Object Type: %s\n", eb->type);
  printf("Object Data: %s\n", eb->data);
  printf("Object Size: %dx%d\n", eb->width, eb->height);
  printf("\n");

  return TRUE;
}

void encompass_url_requested(GtkHTML *html,
			     gchar * url, GtkHTMLStream * stream,
			     Encompass_Objects * browser) {
  ne_session *sess;
  ne_request * request;
  HTMLURL * tmpurl;
  gchar * real_url;

  real_url = encompass_parse_href(url);
  tmpurl = html_url_new(real_url);
  
  printf ("mime type: %s\n", gnome_vfs_mime_type_from_name (real_url));

  if(!strncasecmp(real_url, "file:", strlen("file:"))) {
    encompass_load_file(real_url, stream);
  } else {
    if(tmpurl->port == 0 && !strcasecmp(tmpurl->protocol, "http")) {
      tmpurl->port = 80;
    } else if(tmpurl->port == 0 && !strcasecmp(tmpurl->protocol, "https")) {
      tmpurl->port = 443;
    }

    sess = ne_session_create();
    
    ne_session_server(sess, tmpurl->hostname, tmpurl->port);
    ne_set_useragent(sess, "Mozilla/5 compatible; Encompass 1.0");
    ne_set_secure(sess, TRUE);
    request = ne_request_create(sess, "GET", tmpurl->path);

    ne_add_response_body_reader(request, ne_accept_always,
				  (void *)encompass_read_block, stream);

    ne_request_dispatch(request);
    ne_request_destroy(request);
    ne_session_destroy(sess);
    gtk_html_end(GTK_HTML(html), stream, GTK_HTML_STREAM_OK);
  }
  html_url_destroy(tmpurl);
}

void  encompass_link_clicked(GtkHTML * html, gchar * url, 
			     Encompass_Objects * browser) {
  browser->handle = gtk_html_begin (html);
  encompass_goto_url (html, g_strdup (url), browser->handle, browser);
}
