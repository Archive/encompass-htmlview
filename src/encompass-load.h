#ifndef _EMCOMPASS_LOAD_H_
#define _EMCOMPASS_LOAD_H_

void encompass_ps_load (BonoboPersistStream * ps,
			Bonobo_Stream stream,
			Bonobo_Persist_ContentType type,
			Encompass_Objects * browser,
			CORBA_Environment * ev);

Bonobo_Persist_ContentTypeList *
encompass_ps_types (BonoboPersistStream * ps,
		    Encompass_Objects * browser,
		    CORBA_Environment * ev);

void encompass_read_block(GtkHTMLStream * stream,
			    const char * buf,
			    size_t len);

gchar * encompass_parse_href (const gchar *s);

void encompass_load_file (const gchar * url, GtkHTMLStream * handle);

#endif
