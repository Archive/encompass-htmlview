#include "encompass.h"

enum {
	ARG_0,
	ARG_URL,
	ARG_TITLE
};

static gint refcount = 0;

static void
get_prop (BonoboPropertyBag * bag, BonoboArg * arg, guint arg_id, 
	  CORBA_Environment *ev, gpointer data) {
  switch (arg_id) {
  case ARG_URL:
    break;
  case ARG_TITLE:
    break;
  default:
    break;
  }
  g_print ("arg %d queried\n", arg_id);
}

void encompass_goto_url (GtkHTML * html, gchar * url,
			 GtkHTMLStream * stream,
			 Encompass_Objects * browser) {
  gchar * real_url = encompass_parse_href (url);
  encompass_set_base (html, g_strdup (real_url), browser);
  if(!strchr(real_url, '?') && 
     !strncmp(gnome_mime_type(real_url), "image/", strlen("image/"))) {
    static gchar * img_page;
    img_page = g_strconcat("<title>Image: ",
			   g_strdup (real_url),
			   "</title>\n<img src=\"",
			   g_strdup (real_url), "\">\n", NULL);
    gtk_html_write (html, stream,
		    g_strdup (img_page), strlen(img_page));
    g_free (img_page);
    gtk_html_end (html, stream, GTK_HTML_STREAM_OK);
  } else {
    encompass_url_requested(html, g_strdup (real_url), stream, browser);
  }
  g_free (real_url);
}

static void
set_prop (BonoboPropertyBag * bag, const BonoboArg * arg, guint arg_id, 
	  CORBA_Environment *ev, Encompass_Objects * browser) {
  switch (arg_id) {
  case ARG_URL:
    {
      gchar * url = BONOBO_ARG_GET_STRING (arg);
      browser->handle = gtk_html_begin (GTK_HTML (browser->html));
      encompass_goto_url (GTK_HTML (browser->html), g_strdup (url),
			  browser->handle, browser);
      g_free (url);
    }
    break;
  case ARG_TITLE:
    gtk_object_set(GTK_OBJECT(browser->html), "title",
		   BONOBO_ARG_GET_STRING(arg), NULL);
    break;
  default:
    g_print ("Arg %d set\n", arg_id);
    break;
  }
}

static void
control_destroy_cb (BonoboControl * control, gpointer data)
{
	bonobo_object_unref (BONOBO_OBJECT (data));
	if (--refcount < 1)
		gtk_main_quit ();
}

static BonoboObject *
encompass_factory (BonoboGenericFactory * factory, void * closure)
{
	BonoboControl * control;
	BonoboPersistStream * stream;
	GtkWidget * w;
	Encompass_Objects * browser;

	browser = g_new0(Encompass_Objects, 1);

	w = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (w),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_widget_show (w);

	browser->html = gtk_html_new();
	gtk_container_add(GTK_CONTAINER(w), browser->html);

	gtk_signal_connect (GTK_OBJECT (browser->html), "title_changed",
			    GTK_SIGNAL_FUNC (encompass_title_changed),
			    browser);
	gtk_signal_connect (GTK_OBJECT (browser->html), "object_requested",
			    GTK_SIGNAL_FUNC (encompass_object_requested),
			    browser);
	gtk_signal_connect (GTK_OBJECT (browser->html), "link_clicked",
			    GTK_SIGNAL_FUNC (encompass_link_clicked),
			    browser);
	gtk_signal_connect (GTK_OBJECT (browser->html), "url_requested",
			    GTK_SIGNAL_FUNC (encompass_url_requested),
			    browser);

	gtk_widget_show(browser->html);
	
	control = bonobo_control_new (w);

	bonobo_control_set_automerge (control, TRUE);

	if (!control) {
		gtk_object_destroy (GTK_OBJECT (w));
		return NULL;
	}

	stream = bonobo_persist_stream_new (encompass_ps_load, NULL, 
					    NULL, encompass_ps_types,
					    browser);
	bonobo_object_add_interface (BONOBO_OBJECT (control),
				     BONOBO_OBJECT (stream));

	browser->pb = bonobo_property_bag_new (get_prop, set_prop, browser);
	bonobo_control_set_properties (control, browser->pb);

	bonobo_property_bag_add (browser->pb, "url", ARG_URL,
				 BONOBO_ARG_STRING,
				 NULL, "Main URL to display", 0);
	bonobo_property_bag_add (browser->pb, "title", ARG_TITLE,
				 BONOBO_ARG_STRING,
				 NULL, "Title of page", 0);

	bonobo_object_add_interface (BONOBO_OBJECT (control), 
				     BONOBO_OBJECT (browser->pb));

	gtk_signal_connect (GTK_OBJECT (w), "destroy",
			    control_destroy_cb, control);

	refcount++;

	return BONOBO_OBJECT (control);
}

static gboolean
encompass_factory_init (void)
{
  static BonoboGenericFactory * enfact = NULL;

  if (!enfact) {
    enfact = bonobo_generic_factory_new ("OAFIID:GNOME_Encompass_HTMLView_Factory",
					 encompass_factory, NULL);
    if (!enfact)
      g_error ("Cannot create encompass-htmlview factory");
  }
  return FALSE;
}

int main (int argc, gchar ** argv)
{
#ifdef GTKHTML_HAVE_GCONF
	GError  *gconf_error  = NULL;
#endif
	CORBA_Environment ev;
	CORBA_ORB orb;

	CORBA_exception_init (&ev);

	gnome_init_with_popt_table ("EncompassHTMLView", VERSION,
				    argc, argv,
				    oaf_popt_options, 0, NULL);

	orb = oaf_init (argc, argv);

	if (bonobo_init (orb, NULL, NULL) == FALSE)
		g_error ("Couldn't initialize Bonobo");

#ifdef GTKHTML_HAVE_GCONF
	if (!gconf_init (argc, argv, &gconf_error)) {
		g_assert (gconf_error != NULL);
		g_error ("GConf init failed:\n  %s", gconf_error->message);
		return 1;
	}
#endif

	gdk_rgb_init ();
	gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
	gtk_widget_set_default_visual (gdk_rgb_get_visual ());

	gtk_idle_add ((GtkFunction) encompass_factory_init, NULL);

	bonobo_main ();

	return 0;
}

