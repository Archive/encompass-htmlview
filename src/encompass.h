#ifndef _ENCOMPASS_H_
#define _ENCOMPASS_H_

#include <ne_request.h>
#include <ne_basic.h>
#include <ne_redirect.h>
#include <ne_utils.h>
#include <ne_socket.h>
#include <ne_uri.h>

#undef PACKAGE
#undef VERSION

#include <config.h>
#include <gnome.h>
#include <bonobo.h>

#include <gtkhtml/gtkhtml.h>

typedef struct {
  GtkWidget *html;
  GtkWidget *scroll;
  GtkHTMLStream *handle;
  BonoboPropertyBag * pb;
} Encompass_Objects;

#include "htmlurl.h"
#include "print.h"
#include "search.h"
#include "encompass-load.h"
#include "encompass-widget.h"

HTMLURL * baseURL;

void encompass_goto_url (GtkHTML * html, gchar * url,
			 GtkHTMLStream * stream,
			 Encompass_Objects * browser);

#endif
