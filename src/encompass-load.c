#include "encompass.h"
#include <gtkhtml/gtkhtml-stream.h>
#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>

#define READ_CHUNK_SIZE 4096

void encompass_ps_load (BonoboPersistStream * ps,
		       Bonobo_Stream stream,
		       Bonobo_Persist_ContentType type,
		       Encompass_Objects * browser,
		       CORBA_Environment * ev)
{
	Bonobo_Stream_iobuf * buffer;

	if (strcmp (type, "text/html") != 0) {
		CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
				     ex_Bonobo_Persist_WrongDataType, NULL);
		return;
	}

	browser->handle = gtk_html_begin(GTK_HTML(browser->html));

	do {
		Bonobo_Stream_read (stream, READ_CHUNK_SIZE,
				    &buffer, ev);
		if (ev->_major != CORBA_NO_EXCEPTION) break;
		if (buffer->_length <= 0) break;
		gtk_html_write (GTK_HTML(browser->html), browser->handle,
				buffer->_buffer,
				buffer->_length);
		CORBA_free (buffer);
	} while (1);

	if (ev->_major != CORBA_NO_EXCEPTION) {
		gtk_html_stream_close(browser->handle, GTK_HTML_STREAM_ERROR);
	} else {
		CORBA_free (buffer);
		gtk_html_stream_close (browser->handle, GTK_HTML_STREAM_OK);
	}
}

Bonobo_Persist_ContentTypeList *
encompass_ps_types (BonoboPersistStream * ps,
		   Encompass_Objects * browser,
		   CORBA_Environment * ev)
{
	return bonobo_persist_generate_content_types (1, "text/html");
}


void encompass_read_block(GtkHTMLStream * stream,
			    const char * buf,
			    size_t len)
{
  if (len) {
    gtk_html_stream_write(stream, buf, len);
  }
}

gchar * encompass_parse_href (const gchar *s)
{
	gchar *retval;
	gchar *tmp;
	HTMLURL *tmpurl;

	if(s == NULL || *s == 0)
		return NULL;

	if (s[0] == '#') {
		tmpurl = html_url_dup (baseURL, HTML_URL_DUP_NOREFERENCE);
		html_url_set_reference (tmpurl, s + 1);

		tmp = html_url_to_string (tmpurl);
		html_url_destroy (tmpurl);

		return tmp;
	}

	tmpurl = html_url_new (s);
	if (html_url_get_protocol (tmpurl) == NULL) {
		if (s[0] == '/') {
			if (s[1] == '/') {
				gchar *t;

				/* Double slash at the beginning.  */

				/* FIXME?  This is a bit sucky.  */
				t = g_strconcat (html_url_get_protocol (baseURL),
						 ":", s, NULL);
				html_url_destroy (tmpurl);
				tmpurl = html_url_new (t);
				retval = html_url_to_string (tmpurl);
				html_url_destroy (tmpurl);
				g_free (t);
			} else {
				/* Single slash at the beginning.  */

				html_url_destroy (tmpurl);
				tmpurl = html_url_dup (baseURL,
						       HTML_URL_DUP_NOPATH);
				html_url_set_path (tmpurl, s);
				retval = html_url_to_string (tmpurl);
				html_url_destroy (tmpurl);
			}
		} else {
			html_url_destroy (tmpurl);
			tmpurl = html_url_append_path (baseURL, s);
			retval = html_url_to_string (tmpurl);
			html_url_destroy (tmpurl);
		}
	} else {
		retval = html_url_to_string (tmpurl);
		html_url_destroy (tmpurl);
	}

	return retval;
}

void encompass_load_file (const gchar * url, GtkHTMLStream * handle) {
  int fp;
  gchar * buffer;
  struct stat stbuf;

  if(!lstat(url + 5, &stbuf)) {
    if (opendir (url + 5)) {
      buffer = g_strdup_printf ("ERROR: %s is a directory.\n", url);
      gtk_html_stream_write (handle, g_strdup (buffer), strlen (buffer));
      g_free(buffer);
      gtk_html_stream_close (handle, GTK_HTML_STREAM_OK);
      return;
    } else if ((fp = open(url + 5, O_RDONLY | O_NONBLOCK))) {
      buffer = (char *)mmap(NULL, stbuf.st_size, PROT_READ,
			    MAP_SHARED, fp, 0);

      gtk_html_stream_write(handle, buffer, stbuf.st_size);
      close(fp);
      munmap(buffer, stbuf.st_size);

      gtk_html_stream_close (handle, GTK_HTML_STREAM_OK);
      return;
    }
  }
}
