#ifndef _ENCOMPASS_WIDGET_H_
#define _ENCOMPASS_WIDGET_H_

#include <fcntl.h>

void encompass_title_changed(GtkHTML * html,
			     const gchar * title,
			     Encompass_Objects * browser);
void encompass_set_base (GtkHTML * html,
			 gchar * base,
			 Encompass_Objects * browser);
gboolean encompass_object_requested(GtkHTML * html,
				   GtkHTMLEmbedded * eb,
				   Encompass_Objects * browser);
void encompass_link_clicked(GtkHTML * html,
			    gchar * url,
			    Encompass_Objects * browser);
void encompass_url_requested(GtkHTML *html,
			     gchar * url, GtkHTMLStream * stream,
			     Encompass_Objects * browser);


#endif
