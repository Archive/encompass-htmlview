dnl Process this file with autoconf to produce a configure script.

AC_INIT(configure.in)
AM_INIT_AUTOMAKE(encompass-htmlview, 0.4.99.1)
AM_CONFIG_HEADER(config.h)

dnl Pick up the Gnome macros.
AM_ACLOCAL_INCLUDE(macros)

AC_CANONICAL_HOST
GNOME_INIT
AC_ISC_POSIX
AC_PROG_CC
AM_PROG_CC_STDC
AC_HEADER_STDC

GNOME_COMPILE_WARNINGS
GNOME_X_CHECKS

dnl Add the languages which your application supports here.
ALL_LINGUAS=""
AM_GNU_GETTEXT

dnl Neon Library checks
AC_MSG_CHECKING(for neon >= 0.15.0)
if neon-config --libs > /dev/null 2>&1; then
	vers=`neon-config --version | sed -e "s/neon //" -e 's/cvs$//' -e 's/pre$//' | \
	awk 'BEGIN { FS = "."; } { print $1 * 1000 + $2;}'`
	if test "$vers" -ge 15; then
		AC_MSG_RESULT(found)
		NEON_CFLAGS=`neon-config --cflags`
		NEON_LIBS=`neon-config --libs`
		AC_SUBST(NEON_CFLAGS)
		AC_SUBST(NEON_LIBS)
	else
		AC_MSG_ERROR(You need at least neon 0.15.0 for this version of the Encompass HTMLView)
	fi
else
	AC_MSG_ERROR(Did not find neon installed)
fi

dnl ******************************
dnl Gnome App Lib checking
dnl ******************************
AC_MSG_CHECKING(for Gnome App libraries (GAL) >= 0.7)
if gnome-config --libs gal > /dev/null 2>&1; then 
    vers=`gnome-config --modversion gal | sed -e "s/gal-//" -e 's/cvs$//' -e 's/pre$//' | \
        awk 'BEGIN { FS = "."; } { print $1 * 1000 + $2;}'`
    if test "$vers" -ge 5; then
        AC_MSG_RESULT(found)
    else
        AC_MSG_ERROR(You need at least GNOME Application libs 0.7 for this version of the Encompass HTMLView)
    fi
else
    AC_MSG_ERROR(Did not find GnomeAppLib (GAL) installed)
fi

dnl Bonobo checks.

try_bonobo=true
bonobo=
bonobo_msg=no
have_bonobo=false

AC_ARG_WITH(bonobo,
	[--{with,without}-bonobo   Compile with Bonobo support or without it],
	if test x$withval = xno; then
		try_bonobo=false
	fi
	if test x$withval = xyes; then
		try_bonobo=true
	fi
)

if $try_bonobo; then
	AC_MSG_CHECKING(for Bonobo >= 0.36)

	if gnome-config --libs bonobox > /dev/null 2>&1; then
		vers=`gnome-config --modversion bonobo | sed -e "s/bonobo-//" | \
			awk 'BEGIN { FS = "."; } { print $1 * 1000 + $2;}'`
		if test "$vers" -ge 36; then
			bonobo_ok=true
		else
			bonobo_ok=false
		fi
	else
		bonobo_ok=false
	fi
	
	if $bonobo_ok; then
		AC_MSG_RESULT([found ("$vers")])
		AC_DEFINE(ENABLE_BONOBO)
		have_bonobo=true
		if gnome-config --libs bonobox > /dev/null 2>&1; then
			bonobo=bonobox
		else
			bonobo=bonobo
		fi
		bonobo_msg=yes
	else
		AC_MSG_ERROR([not found, you need bonobo to compile the Encompass HTMLView])
	fi
fi

AM_CONDITIONAL(BONOBO, $have_bonobo)

dnl Check if Bonobo is OAFized
AC_MSG_CHECKING(if Bonobo uses OAF)
if ( gnome-config --libs bonobo | grep oaf ) > /dev/null 2>&1 ; then
	using_oaf="yes"
	AC_DEFINE(USING_OAF)
else
	using_oaf="no"
fi

AC_MSG_RESULT("$using_oaf")

AM_CONDITIONAL(USING_OAF, test "x$using_oaf" = "xyes")

dnl ******************************
dnl GnomePrint checking
dnl ******************************
AC_MSG_CHECKING(for GnomePrint libraries >= 0.24)
if gnome-config --libs print > /dev/null 2>&1; then 
    vers=`gnome-config --modversion print | sed -e "s/gnome-print-//" -e 's/cvs$//' -e 's/pre$//' | \
        awk 'BEGIN { FS = "."; } { print $1 * 1000 + $2;}'`
    if test "$vers" -ge 24; then
        AC_MSG_RESULT(found)
    else
        AC_MSG_ERROR(You need at least GNOME print 0.24 for this version of the Encompass HTMLView)
    fi
else
    AC_MSG_ERROR(Did not find GnomePrint installed)
fi


dnl Check for GtkHTML

AC_MSG_CHECKING([for GtkHTML >= 0.9.2])

if gnome-config --libs gtkhtml > /dev/null 2>&1; then
	vers=`gnome-config --modversion gtkhtml | sed 's/^gtkhtml-//'`
        major=`echo "$vers" | sed 's/\([[0-9]]*\)\.[[0-9]]*\.[[0-9]]*/\1/'`
        minor=`echo "$vers" | sed 's/[[0-9]]*\.\([[0-9]]*\)\.[[0-9]]*/\1/'`

	if [[ "x$major" = "x" ]] ; then
		gtkhtml_ok=false
        elif [[ "x$major" = "x0" ]] ; then
		case "$minor" in
			[[012345678]])
				gtkhtml_ok=false ;;
                        *)
				gtkhtml_ok=true ;;
		esac
	else
		gtkhtml_ok=true
	fi
	AC_MSG_RESULT([found ("$vers")])
	GTKHTML_LIBS=`gnome-config --libs gtkhtml`
	GTKHTML_CFLAGS=`gnome-config --cflags gtkhtml`
	AC_SUBST(GTKHTML_LIBS)
	AC_SUBST(GTKHTML_CFLAGS)
else
	gtkhtml_ok=false
	AC_MSG_RESULT([not found])
fi

if test x$gtkhtml_ok = xfalse ; then
	AC_MSG_ERROR([GtkHTML 0.9.2 or later is required to compile the Encompass HTMLView.])
fi

dnl Setup extra GNOME library flags.

EXTRA_GNOME_LIBS=`gnome-config --libs gnomeui gdk_pixbuf print gal $bonobo vfs`
EXTRA_GNOME_CFLAGS=`gnome-config --cflags gnomeui gdk_pixbuf print gal $bonobo vfs`

AC_SUBST(EXTRA_GNOME_LIBS)
AC_SUBST(EXTRA_GNOME_CFLAGS)

dnl Set PACKAGE_LOCALE_DIR in config.h.
if test "x${prefix}" = "xNONE"; then
  AC_DEFINE_UNQUOTED(PACKAGE_LOCALE_DIR, "${ac_default_prefix}/${DATADIRNAME}/locale")
else
  AC_DEFINE_UNQUOTED(PACKAGE_LOCALE_DIR, "${prefix}/${DATADIRNAME}/locale")
fi

AC_MSG_CHECKING([whether to install into Gnome's prefix])
if test x"$use_system_install" = xyes ; then
	AC_MSG_RESULT([yes])
	gnomedatadir=`${GNOME_CONFIG} --datadir`
	gnomeconfdir=`${GNOME_CONFIG} --sysconfdir`
else
	AC_MSG_RESULT([no])
	gnomedatadir=`eval "echo ${datadir}"`
	gnomeconfdir=`eval "echo ${sysconfdir}"`
fi

AC_SUBST(gnomedatadir)
AC_SUBST(gnomeconfdir)

GNOME_HELP_DIR="`gnome-config --datadir`/help"
AC_SUBST(GNOME_HELP_DIR)

dnl Subst PACKAGE_PIXMAPS_DIR.
PACKAGE_PIXMAPS_DIR="`gnome-config --datadir`/pixmaps/${PACKAGE}"
AC_SUBST(PACKAGE_PIXMAPS_DIR)

AC_OUTPUT([
Makefile
dnl encompass.spec
intl/Makefile
src/Makefile
src/GNOME_Encompass_HTMLView.oaf
po/Makefile.in
])

